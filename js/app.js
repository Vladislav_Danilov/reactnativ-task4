let {
    Router,
    Route,
    IndexRoute,
    IndexLink,
    Link
} = ReactRouter;

let arrayItems = [
    {
        name: 'Товар 1',
        price: 100
    },
    {
        name: 'Товар 2',
        price: 600
    },
    {
        name: 'Товар 3',
        price: 200
    },
    {
        name: 'Товар 4',
        price: 300
    },
    {
        name: 'Товар 5',
        price: 500
    }
];

window.ee = new EventEmitter();

let basketItems = [];

let DeliveryCondition = React.createClass({
        propTypes: {
            sum: React.PropTypes.number
        },
        render: function () {
            let sum = this.props.sum;
            if (sum > 1000) {
                return (
                    <div className="delivery_condition">
                        Доставка: бесплатно
                    </div>
                )
            }
            if (sum > 500) {
                return (
                    <div className="delivery_condition">
                        Доставка: 100p
                    </div>
                )
            }
            return (
                <div className="delivery_condition">
                    Доставка: 200p
                </div>
            )
        }
    }
)

let BasketItems = React.createClass({
    propTypes: {
        data: React.PropTypes.array.isRequired
    },
    render: function () {
        let data = this.props.data;
        let items = data.map(function (item, index) {
            console.log(item);
            return (
                <div className="basket_item" key={index}>
                    <p className="name_and_price">{item.name + " x " + item.price * item.number}</p>
                </div>
            )
        });
        return (
            <div className="list_basket_items">
                {items}
            </div>
        );
    }
});

let Sum = React.createClass({
    propTypes: {
        data: React.PropTypes.array.isRequired
    },
    render: function () {
        let data = this.props.data;
        let sumCount = 0;
        data.map(function (item) {
            sumCount = sumCount + item.price * item.number;
        });
        return (
            <div className="sum">
                Всего: {sumCount}
                <DeliveryCondition sum={sumCount}/>
            </div>
        );
    }
});

function getIndexBasketItem(itemName) {
    for (let i = 0; i < basketItems.length; i++) {
        if (basketItems[i].name == itemName) {
            return i;
        }
    }
    return -1;
}

let Basket = React.createClass({
    getInitialState: function () {
        return {
            items: basketItems
        };
    },
    componentDidMount: function () {
        let self = this;
        window.ee.addListener('Item.add', function (item) {
            let index = getIndexBasketItem(item.name);
            if (index > -1) {
                basketItems[index].number = basketItems[index].number + 1;
            } else {
                let basketItem = {};
                basketItem.name = item.name;
                basketItem.price = item.price;
                basketItem.number = 1;
                basketItems.push(basketItem);
            }
            self.setState({items: basketItems});
        });
    },
    componentWillUnmount: function () {
        window.ee.removeListener('Item.add');
    },
    render: function () {
        return (
            <div className="basket">
                <table className="basket_table">
                    <tbody>
                    <tr>
                        <td>
                            <p className="title_basket">Basket</p>
                            <BasketItems data={this.state.items}/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Sum data={this.state.items}/>
                            <div className="order_sum">
                                <Link className="button" to='/confirm'>Заказать</Link>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
});

let ListItems = React.createClass({
    propTypes: {
        data: React.PropTypes.array.isRequired
    },
    onBtnClickHandler: function (index) {
        console.log(index);
        window.ee.emit('Item.add', this.props.data[index]);
    },
    render: function () {
        let data = this.props.data;
        let self = this;
        let items = data.map(function (item, index) {
            return (
                <div className="cell_item" key={index}>
                    <p className="name_and_price">{item.name + ", " + item.price + "p"}</p>
                    <button onClick={() => self.onBtnClickHandler(index)}>Добавить в корзину</button>
                </div>
            )
        });
        return (
            <div className="list_items">
                {items}
            </div>
        );
    }
});

let Home = React.createClass({
    render: function () {
        return (
            <table className="shop_details_container">
                <tbody>
                <tr>
                    <td className="list_items_container">
                        <ListItems data={arrayItems}/>
                    </td>
                    <td className="basket_container">
                        <Basket/>
                    </td>
                </tr>
                </tbody>
            </table>
        );
    }
});

let Confirm = React.createClass({
    render: function () {
        return (
            <div className="confirm_container">
                <BasketItems data={basketItems}/>
                <Sum data={basketItems}/>
                <a className="button" a href="">Подтвердить</a>
            </div>
        );
    }
});

let NotFound = React.createClass({
    render: function () {
        return (
            <div>
                Страница не найдена.
                <Link to='/'>Вернуться на главную</Link>
            </div>
        );
    }
});

let App = React.createClass({
    render: function () {
        return (
            <div>
                <header>Shop</header>
                <article>
                    {this.props.children}
                </article>
                <footer>ReactJS: task four &copy;</footer>
            </div>
        );
    }
});

ReactDOM.render(
    <Router>
        <Route path='/' component={App}>
            <IndexRoute component={Home}/>
            <Route path="confirm" component={Confirm}/>
            <Route path="*" component={NotFound}/>
        </Route>
    </Router>,
    document.getElementById('root')
);